

import QtQuick 2.12
import QtQuick.Window 2.12
import QtMultimedia 5.12

Window {
    id: mainwindow
    visible: true
    // width: Screen.width; height: Screen.height;
    width: 320; height: 568;
    color: "#2A2823"

    property string url: "https://radio.lalibre.net/radiojatarikichwa"
    property string footerUrl: "https://lalibre.net"

    Text{
        id: mainMsg
        color: "#f3f2f2"
        font.family: "Arial"; font.bold: true; font.pixelSize: 32;
        text: "Toca para iniciar"
        topPadding: 50
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        width: parent.width;
        horizontalAlignment: Text.AlignHCenter

        Text {
            id: statusMsg
            color: "#f8c150"
            font.family: "Arial"; font.pixelSize: 20;
            text: qsTr("En espera")
            topPadding: 130
            verticalAlignment: Text.AlignVCenter
            width: parent.width;
            horizontalAlignment: Text.AlignHCenter
        }

    }


    Audio {
        id: playMusic
        source: url
        onPlaybackStateChanged: {

            switch (status){
            case 1:
                // NoMedia - no media has been set
                statusMsg.text = "Revisa tu conexión"
                break
            case 2:
                // Loading - the media is currently being loaded"
                statusMsg.text = "Cargando"
                break
            case 3:
                // Loaded - the media has been loaded
                statusMsg.text = "Reproduciendo"
                break
            case 4:
                // Buffering - the media is buffering data
                statusMsg.text = "Cargando"
                break
            case 5:
                // Stalled - playback has been interrupted while the media is buffering data
                statusMsg.text = "Revisa tu conexión"
                break
            case 6:
                // Buffered - the media has buffered data
                statusMsg.text = "Reproduciendo"
                break
            case 7:
                // EndOfMedia - the media has played to the end
                statusMsg.text = "Revisa tu conexión"
                break
            case 8:
                // InvalidMedia - the media cannot be played
                statusMsg.text = "Revisa tu conexión"
                break
            case 9:
                // UnknownStatus - the status of the media is unknown
                statusMsg.text = "Revisa tu conexión"
                break
            }

            console.log("STATUS: \t" + status)

        }
        onErrorChanged: {
            statusMsg.text = "Revisa tu conexión"
            console.log("ERROR: \t" + errorString)
        }

    }


    Image {
        id: medialogo
        x: 292
        y: 75
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        clip: false
        source: "logo.png"
        width: 160; height: 144;
    }

    Image {
        id: footerlogo
        width: 131; height: 32;
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        fillMode: Image.PreserveAspectFit
        source: "footerlogo.png"
    }

    MouseArea {
        id: footermouse
        width: parent.width;
        height: footerlogo.height + 30;
        anchors.bottom: parent.bottom
        onClicked: {
            Qt.openUrlExternally(footerUrl)
        }
    }

    MouseArea {
        id: playArea
        width: parent.width; height: parent.height - footerlogo.height;
        onClicked: {
            if (playMusic.playbackState === 1){
                playMusic.stop();
                mainMsg.text =  "Toca para escuchar";
                statusMsg.text = "Detenido"
            }
            else{
                playMusic.play();
                mainMsg.text = "Toca para detener";
            }

            console.log("AUDIO PBS: \t" + playMusic.playbackState);

        }
    }




}

