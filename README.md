# LaLibre Radio

Esta es una aplicación para reproducción de flujos de audio increíblemente simple y muy sencilla desarrollada en solo 57 líneas de código (main.qml) gracias a Qt / QtCreator usando el lenguaje QML.

Pretende servir como base para el desarrollo de aplicaciones móbiles multiplataforma para medios y radios comunitarias.

**REVISA Y PRIORIZA LAS [ISSUES PROPUESTAS HASTA EL MOMENTO](https://gitlab.com/lalibre/lalibre-radio/-/boards), VOTA POR ELLAS PARA EMPUJAR SU PRIORIZACIÓN Y DESARROLLO.**

<img src="https://gitlab.com/lalibre/lalibre-radio/-/wikis/uploads/d597e01956a5e75561b6a5521b14ce09/Screenshot_from_2020-08-07_18-43-15.png" alt="LaLibre Radio"/>

## Compilación

### Android

Solo es necesario descomentar la línea 20 del archivo `lalibre-radio.pro` y construye la app con QtCreator.

```
# The next line is for android run and debug
QMAKE_LINK    = $$QMAKE_CXX $$QMAKE_CFLAGS -Wl,--exclude-libs,libgcc.a -Wl,--exclude-libs,libatomic.a -nostdlib++

```

### Linux

Comenta la línea 20 del archivo `lalibre-radio.pro` y construye la app con QtCreator.

```
# The next line is for android run and debug
# QMAKE_LINK    = $$QMAKE_CXX $$QMAKE_CFLAGS -Wl,--exclude-libs,libgcc.a -Wl,--exclude-libs,libatomic.a -nostdlib++

```

### Windows, MAC, iOS

No documentado aún, pendiente para próximas versiones.
